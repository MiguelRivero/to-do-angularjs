/**
 * Created by Miguel Rivero on 10/11/2014.
 * Last update on 19/12/2014
 */

var todo_controller = angular.module('todo_controller',[]);

todo_controller.controller("todo_ctlr",['$scope', function($scope){
    // todos
    var defaultTodos = [
        { title:'Buy Christmas presents', done: false},
        { title: "Watch 'How I met your mother'", done: true }
    ];

    $scope.todos = JSON.parse( localStorage.todos || null ) || angular.copy(defaultTodos);
    $scope.listName = "To Do List";
    $scope.addTodo =  function (new_todo){
        if(new_todo != null || new_todo != undefined ){
            if(new_todo.title.length>2){
                new_todo.done = false;
                var todo = angular.copy(new_todo);
                $scope.todos.push(todo);
                clear();
            }
        }
    };

    $scope.todoDone = save();

    $scope.delete = function(index_to_remove){
        $scope.todos.splice(index_to_remove, 1);
    };

    function clear(){
        $scope.new_todo = undefined;
    };

    function save(){
        localStorage.todos = JSON.stringify($scope.todos);
    };
    var saveState = setInterval(save,2000);

}]);