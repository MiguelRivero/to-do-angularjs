/**
 * Created by Miguel Rivero on 10/11/2014.
 * Last update on 19/12/2014
 */

var todo_app = angular.module('todo_app',[
    'todo_controller'
]);

todo_app.directive('setFocusIf', function($timeout) {
  return {
    link: function($scope, $element, $attr) {
      $scope.$watch($attr.setFocusIf, function(value) {
        if ( value ) {
          $timeout(function() {
            // We must reevaluate the value in case it was changed by a subsequent
            // watch handler in the digest.
            if ( $scope.$eval($attr.setFocusIf) ) {
              $element[0].focus();
            }
          }, 0, false);
        }
      });
    }
  }
});
